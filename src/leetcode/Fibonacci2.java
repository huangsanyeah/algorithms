package leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author huangweiyue
 * @version v1.0
 * @task 剑指 Offer 10- I. 斐波那契数列 https://leetcode-cn.com/problems/fei-bo-na-qi-shu-lie-lcof/
 * @description 斐波那契数
 * 斐波那契数列由 0 和 1 开始，之后的斐波那契数就是由之前的两数相加而得出。
 *
 * 答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/fei-bo-na-qi-shu-lie-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @date Created in 2021-07-06
 */
public class Fibonacci2 {
    public static void main(String[] args) {
        System.out.println(fib(3));
    }

    static Map<Integer, Integer> map = new HashMap<>();
    public static int fib(int n) {
        if (map.containsKey(n)) {
            return map.get(n);
        }
        if (n < 0) {
            return -1;
        }
        if (n == 0 || n == 1) {
            return n;
        }
        int res = fib(n - 1) + fib(n - 2);
        res %= 1000000007;
        map.put(n, res);
        return res;
    }
}

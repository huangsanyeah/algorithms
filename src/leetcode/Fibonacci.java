package leetcode;

/**
 * @author huangweiyue
 * @version v1.0
 * @task 509 https://leetcode-cn.com/problems/fibonacci-number/
 * @description 斐波那契数
 * 斐波那契数，通常用 F(n) 表示，形成的序列称为 斐波那契数列 。该数列由 0 和 1 开始，后面的每一项数字都是前面两项数字的和。也就是：
 * F(0) = 0，F(1) = 1
 * F(n) = F(n - 1) + F(n - 2)，其中 n > 1
 * 时间复杂度：递归法：时间复杂度 O(2^n)，即2的n次方
 * @date Created in 2021-07-06
 */
public class Fibonacci {
    public static void main(String[] args) {
        System.out.println(fib(40));
    }

    public static int fib(int n) {
        if (n == 0 || n == 1) {
            return n;
        }
        return fib(n - 1) + fib(n - 2);
    }
}

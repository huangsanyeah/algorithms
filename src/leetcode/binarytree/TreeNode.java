package leetcode.binarytree;

/**
 * @author huangweiyue
 * @description 二叉树遍历Node节点
 * @date Created in 2021-07-06
 */
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

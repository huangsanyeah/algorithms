package leetcode.binarytree;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huangweiyue
 * @version v1.0
 * @task 144 https://leetcode-cn.com/problems/binary-tree-preorder-traversal/
 * @description 二叉树前序遍历
 * 递归的三个步骤实现前序遍历
 * 1.确定递归函数的参数和返回值：
 * 确定哪些参数是递归的过程中需要处理的，那么就在递归函数里加上这个参数， 并且还要明确每次递归的返回值是什么进而确定递归函数的返回类型。
 *
 * 2.确定终止条件：
 * 写完了递归算法, 运行的时候，经常会遇到栈溢出的错误，就是没写终止条件或者终止条件写的不对，操作系统也是用一个栈的结构来保存每一层递归的信息，如果递归没有终止，操作系统的内存栈必然就会溢出。
 *
 * 3.确定单层递归的逻辑：
 * 确定每一层递归需要处理的信息。在这里也就会重复调用自己来实现递归的过程。
 * @date Created in 2021-07-06
 */
public class BinaryTreePreOrderTraversal {
    public static void main(String[] args) {
        //二叉树：[3,9,20,null,null,15,7],
        TreeNode root = new TreeNode(3);
        TreeNode level1Left = new TreeNode(9);
        root.left = level1Left;
        TreeNode level1Right = new TreeNode(20);
        root.right = level1Right;

        TreeNode level2Left = new TreeNode(15);
        level1Right.left = level2Left;
        TreeNode level2Right = new TreeNode(7);
        level1Right.right = level2Right;
        System.out.println(preOrderTraversal(root));

    }

    public static List<Integer> preOrderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<Integer>();
        preorder(root, res);
        return res;
    }

    //1.确定递归函数的参数和返回值
    public static void preorder(TreeNode root, List<Integer> res) {
        //2.确定终止条件 当节点为null的时候表明已经到最后 自然终止
        if (root == null) {
            return;
        }
        //3.确定单层递归的逻辑
        res.add(root.val);
        preorder(root.left, res);
        preorder(root.right, res);
    }
}

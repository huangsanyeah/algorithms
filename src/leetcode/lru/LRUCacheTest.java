package leetcode.lru;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author huangweiyue
 * @version v1.0
 * @task 146 LRU https://leetcode-cn.com/problems/lru-cache/
 * @description
 * @date Created in 2021-07-05
 */
public class LRUCacheTest {
    int capacity;
    LinkedHashMap<Integer, Integer> cache;

    public LRUCacheTest(int capacity) {
        this.capacity = capacity;
        //这里是true
        cache = new LinkedHashMap<Integer, Integer>(capacity, 0.75f, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry eldest) {
                return cache.size() > capacity;
            }
        };
    }

    public static void main(String[] args) {
        LRUCacheTest obj = new LRUCacheTest(3);
//         int param_1 = obj.get(key);
        obj.put(1, 1);
        obj.put(2, 2);
        obj.put(3, 3);
        System.out.println(obj.get(1));
        System.out.println(obj.get(2));
        System.out.println(obj.get(3));
        System.out.println("----------");
        obj.put(4, 4);
        obj.put(5, 5);
        System.out.println(obj.get(1));
        System.out.println(obj.get(2));
        System.out.println(obj.get(3));
        System.out.println(obj.get(4));
        System.out.println(obj.get(5));

    }

    public int get(int key) {
        return cache.getOrDefault(key, -1);
    }

    public void put(int key, int value) {
        cache.put(key, value);
    }

}
